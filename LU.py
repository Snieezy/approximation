import numpy as np


class LU:

    size = 0
    connections = [[]]
    Y = []

    def __init__(self, connections, b, solution):
        self.connections = connections
        self.size = len(connections)
        solution.extend(self.lup_decomposition(b))

    def lup_decomposition(self, b):
        # n, m = self.connections.shape
        # assert n == m, "Matrix is not square!"
        n = self.size
        L = np.zeros((n, n))
        U = np.zeros((n, n))  # macierze zerowe
        P = np.identity(n)

        for k in range(n):
            # wyliczenie k-tej kolumny L jak u Ratajczaka
            for i in range(k, n):
                L[i, k] = self.connections[i, k] - sum([L[i, p] * U[p, k] for p in range(k)])

            # pivoting: index najwiekszego elementu w k-tej kolumnie L
            index = np.argmax(abs(L[k:, k]))
            index += k
            #print("k = ", k)
            # pivoting: zamiany wiersza k-tego z tym z najwiekszym elementem
            self.connections[[index, k]] = self.connections[[k, index]]
            L[[index, k]] = L[[k, index]]
            P[[index, k]] = P[[k, index]]

            # wyliczenie k-tego wiersza U jak u Ratajczaka
            U[k, k] = 1
            assert L[k, k] != 0, "Division by zero!"
            for j in range(k + 1, n):
                U[k, j] = (self.connections[k, j] - sum([L[k, p] * U[p, j] for p in range(k)])) / L[k, k]

            #assert np.dot(L, U) == np.dot(P, A), "Decomposition failure!"
        b = np.dot(P, b)
        self.vector_y(L, b)
        self.vector_x(U)
        return self.vector_x(U)

    def vector_y(self, L, b):
            self.Y = np.zeros(self.size)
            for k in range(0, self.size):
                self.Y[k] = (b[k] - sum([L[k][p] * self.Y[p] for p in range(0, k)])) / L[k][k]

    def vector_x(self, U):
        X = np.zeros(self.size)
        for k in range(self.size - 1, -1, -1):
            X[k] = (self.Y[k] - sum([U[k][j] * X[j] for j in range(k + 1, self.size)]))
        return X