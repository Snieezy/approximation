import numpy as np
import LU


def read_nodes(x, y):
    print("Podaj węzły:")
    for i in range(len(x)):
        text = input()
        split_text = text.split(' ')
        x[i] = float(split_text[0])
        y[i] = float(split_text[1])


def count_polynomial_with_horner_scheme(f, value):
    degree = len(f)
    y = f[0]
    for i in range(1, degree):
        y *= value
        y += f[i]
    return y

def read_checkpoints(f):
    m = int(input("Podaj liczbę zapytań o aproksymowane wartości: "))
    for _ in range(m):
        value = float(input("Podaj wartość: "))
        y = count_polynomial_with_horner_scheme(f, value)
        print(value, y, "\n")
        save_to_file(value, y)



def save_to_file(y, x=" "):
    with open("results.txt", "a") as my_file:
        my_file.write(str(y)+";"+str(x)+"\n")



class Solver:
    s = []
    t = []
    degree = 0
    nodes = 0

    def __init__(self):
        func_number_max = int(input("Podaj liczbę funkcji aproksymowanych: "))
        for _ in range(func_number_max):
            x = self.read_values()
            x.reverse()
            save_to_file(x)
            print(x)
            read_checkpoints(x)

    def read_values(self):
        # file_name = 'test1.txt'
        file_name = 'test2.txt'
        self.degree = int(input("Podaj stopień wielomianu aproksymującego: "))
        save_to_file(self.degree)
        self.nodes = int(input("Podaj liczbę węzłów aproksymacji: "))
        save_to_file(self.nodes)
        x = np.zeros(self.nodes)
        y = np.zeros(self.nodes)
        # read_nodes(x, y)
        i = 0
        with open(file_name) as f:
            for line in f:
                x[i], y[i] = line.split()
                x[i] = float(x[i])
                y[i] = float(y[i])
                i += 1
        return self.count_coefficients(x, y)

    def count_coefficients(self, x, y):
        solution = []
        self.count_coefficient_s(x)
        self.count_coefficient_t(y, x)
        matrix = self.vector_into_matrix()
        LU.LU(matrix, self.t, solution)
        return solution

    def count_coefficient_s(self, x):
        n = 0
        self.s = []
        for i in range(2*self.degree+1):
            value = sum([x[j] ** n for j in range(self.nodes)])
            n += 1
            self.s.append(value)

    def vector_into_matrix(self):
        matrix = np.zeros((self.degree+1, self.degree+1))
        for i in range(self.degree+1):
            matrix[i] = [self.s[m] for m in range(i, self.degree+1+i)]
        return matrix

    def count_coefficient_t(self, y, x):
        n = 0
        self.t = []
        for i in range(self.degree+1):
            value = sum([y[j]*(x[j] ** n) for j in range(self.nodes)])
            self.t.append(value)
            n += 1

